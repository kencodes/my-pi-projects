2) Use mySQLdb cp_reconnect=True to reestablish SQL db connection???
    - TRY THIS: http://stackoverflow.com/questions/207981/how-to-enable-mysql-client-auto-re-connect-with-mysqldb
    - ALSO check mySQL options "interactive_timeout" and "wait_timeout"
    - Ensure auto-reconnect is enabled
        - Issue command again, this should cause a reconnect
    - Ensure I'm not closing the connection
    - Consider using stored SQL procedures where quick response is desired (add dollar, etc.)


Useful info!


verbosity is the debug level:
    verbosity=0 : No debug prints
    verbosity=1 : Some debug prints
    verbosity=2 : MOARR debug prints
    ...         : so far, we only crank it up to two



BUGS [O]=Open; [F]=Fixed; [I]=Invalid [?]=Ready-to-test
[O] Can't post web messages
[O] Turn around bev line in white fridge so solenoid is near output, clean lines, drill out air holes for cold air from freezer
[O] Make new function to handle all the time counter resets in kegbot_main.py
[O] Pi blew up. uProc gets very hot at power up, even without SD Card and shuts down self
        - Maybe it's time to migrate to a real computer running Ubuntu or similar :\
        - Could make USB interface to hardware controller box
        - HW control scheme needs analysis, too. How much current we drawing? Did we overload the uProc GPIO capacity?
[?] When changing a keg, web page and pour reports don't update to new keg until reboot
[O] When SQL Server can't be found, go into vending machine mode
[?] Guest login button needs some serious debounce
[?] When timeout warn is displayed on web i/f and user resets idle timer, message doesn't change from warn
[I] Rename SQL Schema from 'test' to 'kegerator' or similar - was not able to rename schema
[I] Make guest logged in whenever their balance > 0
[I] May need arduino or similar to do pulse counting on flow meter
[F] Add SMTP server and email addresses to YAML to obscure info
[F] When guest bal = 0 and they insert bills, the valve doesn't immediately open, have to log out/in
[F] Add more to logout message including abbreviated pour report
[F] Need better way to quickly update SQL with new balance
[F] Guest balance not printing to terminal correctly when guest first logs in
    * Phil's webpage looks correct though, so maybe not getting data from SQL?
    * If guest times out with incorrect balance showing in terminal, that wrong balance is then written to SQL!
[F] make guest a user that's logged in if balance > 0, but they log out if someone badges in
[F] Give guest access to taps with "ALL" access
[F] Need to update MySQL tables with new volumes, balances...
[F] People without tap access can still increment flow counters...



TO DO:
* Changing pricing depending on employee statsus?
* Rating system
    * If user drinks more than X oz, they get emailed a google doc Form
    * They rate the beer, goes into google doc spreadsheet which MySQL can maybe access?
        * Maybe python can access it?
    * Beers are displayed with average rating and number of ratings
* Advertise new beer - Email everyone with access when a new beer is put on tap



Add features:
* Badge/NFC reader, associate ID numbers with user accounts
* add snarky messages based on amount poured, time poured, etc.

