#!/usr/bin/env python2

import RPi.GPIO as GPIO
import time
import kb_functions
#from datetime import datetime
import datetime
from datetime import timedelta
import os
import MySQLdb
#import sys
import requests
import smtplib
import yaml

os.chdir('/home/pi')
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)


#####################################################
# Open YAML file and save contents to dictionary
#####################################################

# Other kegbot Info is in kegbot_config.yaml
kegbot_config = open("/home/pi/kegbot_config.yaml", 'r')
kegbot_config_yaml = yaml.load(kegbot_config)
kegbot_config.close()


#####################################################
# SQL Database
#####################################################

sql_host = '192.168.201.140' #kegbot_config_yaml['sql_host']
sql_user = kegbot_config_yaml['sql_user']
sql_passwd = kegbot_config_yaml['sql_passwd']
sql_schema = kegbot_config_yaml['sql_schema']
sql_users_table = kegbot_config_yaml['sql_users_table']
sql_kegs_table = kegbot_config_yaml['sql_kegs_table']
sql_taps_table = kegbot_config_yaml['sql_taps_table']
sql_trans_table = kegbot_config_yaml['sql_trans_table']


#####################################################
# WEB SERVER
#####################################################

web_server_ip = kegbot_config_yaml['web_server_ip']
session_timeout_warning = kegbot_config_yaml['session_timeout_warning'] # number of seconds of inactivity before user is warned they will be logged out
session_timeout = kegbot_config_yaml['session_timeout'] # Number of seconds of inactivity after which user is logged out
display_idle_time = kegbot_config_yaml['display_idle_time'] # how long to display logout message before displaying IDLE message on website


#####################################################
# ADMINISTRATOR EMAIL ADDRESSES
#####################################################

admins = kegbot_config_yaml['admins']


#####################################################
# EMAIL SERVER
#####################################################

kb_email_addr = kegbot_config_yaml['kegbot_email_addr']
smtp_server = kegbot_config_yaml['smtp_server'] 


#####################################################
# PIN ASSIGNMENTS FOR BCM NUMBERING in RPi.GPIO MODULE
#####################################################

PIN_SWIPE = kegbot_config_yaml['PIN_SWIPE'] # 24
PIN_GUEST_LOGIN = kegbot_config_yaml['PIN_GUEST_LOGIN'] # 8
PIN_BILLREADER = kegbot_config_yaml['PIN_BILLREADER'] # 11 # BCM pin numbering
valve_pins = kegbot_config_yaml['valve_pins'] # : {1:18, 2:23, 3:25} # maps tap numbers to pin numbers for solenoid valves {tap:pin...}
flow_pins = kegbot_config_yaml['flow_pins'] # : {"tap1":27, "tap2":4, "tap3":17} #maps tap numbers to pin numbers for flow meters {tap:pin...} PINS 4,17 ARE PLACEHOLDERS, NOT ACTUALLY USED


#####################################################
# CONSTANTS
#####################################################

cts_per_oz = kegbot_config_yaml['cts_per_oz'] # : 170.5 # actual value for pulses per oz.
FLOW_METER_BOUNCE_TIME_MS = kegbot_config_yaml['FLOW_METER_BOUNCE_TIME_MS'] # : 50 # only for fake flow meters. The real ones don't need debounce
BILL_READER_BOUNCE_TIME_MS = kegbot_config_yaml['BILL_READER_BOUNCE_TIME_MS'] # : 10
#PIN_SWIPE_BOUNCE_TIME_MS = kegbot_config_yaml['PIN_SWIPE_BOUNCE_TIME_MS']
#PIN_GUEST_LOGIN_BOUNCE_TIME_MS = kegbot_config_yaml['PIN_GUEST_LOGIN_BOUNCE_TIME_MS']

verbosity = kegbot_config_yaml['verbosity'] # : 2 # set greater than 0 to make things print out development messages


#####################################################
# Setup I/O
#####################################################

GPIO.setup(PIN_SWIPE, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(PIN_GUEST_LOGIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(PIN_BILLREADER, GPIO.IN)
GPIO.setup(flow_pins["tap1"], GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(flow_pins["tap2"], GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(flow_pins["tap3"], GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(valve_pins[1], GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(valve_pins[2], GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(valve_pins[3], GPIO.OUT, initial=GPIO.LOW)


#####################################################
# Declare Variables
#####################################################

flow_counters = {1:0, 2:0, 3:0} # {<tapNumber>:<flowCounterValue>,...}
active_session = False
current_user = kb_functions.User
session_time = datetime.datetime.now()
login_time = session_time
session_timed_out = False
badge_id = ''
current_web_message = "idle" # tells program what message is currently displayed in case we need to display different message "idle" "welcome" "warn" "timeout"


#####################################################
# Connect to SQL and mail server
#####################################################

open_kegbot_schema = MySQLdb.connect(sql_host, sql_user, sql_passwd, sql_schema)
open_kegbot_schema.autocommit(True)
kegbot_cursor = open_kegbot_schema.cursor()
mail_server = smtplib.SMTP('mail.nuvation.com')


#####################################################
# Support Functions
#####################################################

def incr_flow_ctr_tap1(derp):
    import random
    global session_time
    global tap_list
    global kegbot_cursor
    global open_kegbot_schema
    if GPIO.input(valve_pins[1]):
        flow_counters[1] += 1
        if current_user.user_id == 1: # Guest pouring beer
            current_user.balance -= tap_list[0].price_per_oz/cts_per_oz
            if current_user.balance <= 0:
                current_user.balance = 0
                enable_disable_taps(True, tap_list, verbosity)
                kb_functions.email_pour_report(current_user, admins, verbosity)
                try:
                    print (kegbot_cursor.execute("UPDATE users SET balance=%.2f WHERE user_id=1;" % current_user.balance)) # user_id=1 for guest account
                except:
                    print "OHNO SOMETHING WENT WRONG WHEN UPDATING SQL GUEST BALANCE!"
        if verbosity>1:
            print "Flow counters now: %s" % str(flow_counters)
    session_time = datetime.datetime.now() # restart session timeout counter


def incr_flow_ctr_tap2(derp):
    import random
    global session_time
    global tap_list
    global kegbot_cursor
    global open_kegbot_schema
    if GPIO.input(valve_pins[2]):
        mimic_flow_meter = random.randint(100,400)
        flow_counters[2] += mimic_flow_meter
        if current_user.user_id == 1: # Guest pouring beer
            current_user.balance -= mimic_flow_meter*tap_list[1].price_per_oz/cts_per_oz
            if current_user.balance <= 0:
                current_user.balance = 0
                enable_disable_taps(True, tap_list, verbosity)
                kb_functions.email_pour_report(current_user, admins, verbosity)
                try:
                    print (kegbot_cursor.execute("UPDATE users SET balance=%.2f WHERE user_id=1;" % current_user.balance)) # user_id=1 for guest account
                except:
                    print "OHNO SOMETHING WENT WRONG WHEN UPDATING SQL GUEST BALANCE!"
        if verbosity>1:
            print "Flow counters now: %s" % str(flow_counters)
    session_time = datetime.datetime.now() # restart session timeout counter


def incr_flow_ctr_tap3(derp):
    import random
    global session_time
    global tap_list
    global kegbot_cursor
    global open_kegbot_schema
    if GPIO.input(valve_pins[3]):
        mimic_flow_meter = random.randint(100,400)
        flow_counters[3] += mimic_flow_meter
        if current_user.user_id == 1: # Guest pouring beer
            current_user.balance -= mimic_flow_meter*tap_list[2].price_per_oz/cts_per_oz
            if current_user.balance <= 0:
                current_user.balance = 0
                enable_disable_taps(True, tap_list, verbosity)
                kb_functions.email_pour_report(current_user, admins, verbosity)
                try:
                    print (kegbot_cursor.execute("UPDATE users SET balance=%.2f WHERE user_id=1;" % current_user.balance)) # user_id=1 for guest account
                except:
                    print "OHNO SOMETHING WENT WRONG WHEN UPDATING SQL GUEST BALANCE!"
        if verbosity>1:
            print "Flow counters now: %s" % str(flow_counters)
    session_time = datetime.datetime.now() # restart session timeout counter


def incr_dollars_inserted(derp):
    import MySQLdb
    global session_time
    global current_user
    global kegbot_cursor
    global open_kegbot_schema
    global sql_schema
    global sql_trans_table
    global active_session
    global login_time
    global tap_list
    
    session_time = datetime.datetime.now() # restart session timeout counter
    # Session should be active when user inserts bills
    if active_session == True:
        current_user.balance += 1
        if current_user.user_id == 1 and current_user.balance == 1:
            enable_disable_taps(False, tap_list, verbosity)
        # Increment user's balance
        (kegbot_cursor.execute("UPDATE users SET balance = balance + 1 WHERE user_id=%s;" % current_user.user_id))
        # Log event
        (kegbot_cursor.execute("""INSERT INTO `%s`.`%s` (`session_time`, `user_id`, `dollars_added`)
            VALUES ('%s', '%s', '%s');""" % (sql_schema, sql_trans_table, login_time, current_user.user_id, 1)))
        try:
            message = """Welcome, %s<br />" +
                "Your remaining balance is $%.2f<br />" +
                "Insert cash when the guest balance falls to $0.00""" % (current_user.user_id, current_user.balance)
            kb_functions.post_web_message(web_server_ip, 'message', message)
            #current_web_message = 'login'
            #kb_functions.post_web_message(web_server_ip, 'login', current_user.user_id)
        except:
            pass
        current_web_message = 'login'
    # If nobody is logged in when bills are inserted, add them to guest account
    else:
        # Open database if not already open
        if not open_kegbot_schema.open:
            open_kegbot_schema = MySQLdb.connect(sql_host, sql_user, sql_passwd, sql_schema)
            kegbot_cursor = open_kegbot_schema.cursor()
        # Increment user's balance
        (kegbot_cursor.execute("UPDATE users SET balance = balance + 1 WHERE user_id=1;"))
        # Log event
        (kegbot_cursor.execute("""INSERT INTO `%s`.`%s` (`session_time`, `user_id`, `dollars_added`)
                    VALUES ('%s', '%s', '%s');""" % (sql_schema, sql_trans_table, login_time, 1, 1)))
        try:
            message = "Please log in first! Money is added to GUEST account when nobody is logged in!"
            kb_functions.post_web_message(web_server_ip, 'message', message)
        except:
            pass
        #current_web_message = ????


def enable_disable_taps(disable_all, tap_list, verbosity): # 
    """ Enables taps based on dictionary returned from get_access()
    Only controls I/O, doesn't return anything
    """
    global current_user
    for i,tap in enumerate(tap_list):
        # No keg on this tap
        if ((tap.keg_id == 1) or (disable_all == True)):
            GPIO.output(valve_pins[i+1], False)
        # Guest is logged in and has zero balance
        elif ((current_user.user_id == 1) and (current_user.balance==0)):
            GPIO.output(valve_pins[i+1], False)
        # Keg is on and everyone gets beer
        elif tap.access == "ALL": # everyone gets beer!
            GPIO.output(valve_pins[i+1], True)
            if verbosity>2:
                print "User gets access to tap %s" % (i+1)
        # Keg is on and only Employees get beer
        elif tap.access == "EMPLOYEES":
            if current_user.is_employee == True:
                GPIO.output(valve_pins[i+1], True)
                if verbosity>2:
                    print "User gets access to tap %s" % (i+1)
        else:
            if verbosity>2:
                print "%s don't get no honey!" % current_user.first_name


def toggle_user_session():
    ''' Is user logging in or out?'''
    global active_session
    global current_user
    global sql_schema
    global sql_users_table
    global open_kegbot_schema
    global kegbot_cursor
    global badge_id
    # Open database if not already open
    if not open_kegbot_schema.open:
        open_kegbot_schema = MySQLdb.connect(sql_host, sql_user, sql_passwd, sql_schema)
        kegbot_cursor = open_kegbot_schema.cursor()
        print "HAD TO RE-OPEN SQL SERVER CONNECTION!!!"
    # Check if logging in or out
    if active_session == False:
        if verbosity>2:
            print "Swipe pressed! Session_active is currently %s. Will now try to open session" % active_session
        # Get badge ID - Normally returned from reader
        #badge_id = raw_input("Enter badge ID (ken=111): ")
        try:
            print (kegbot_cursor.execute("SELECT * FROM %s.%s WHERE badge_id=%s" % (sql_schema, sql_users_table, badge_id)))
        except:
            if not open_kegbot_schema.open:
                open_kegbot_schema = MySQLdb.connect(sql_host, sql_user, sql_passwd, sql_schema)
                kegbot_cursor = open_kegbot_schema.cursor()
                print "HAD TO RE-OPEN SQL SERVER CONNECTION!!!"
            print (kegbot_cursor.execute("SELECT * FROM %s.%s WHERE badge_id=%s" % (sql_schema, sql_users_table, badge_id)))
        sql_search_results = kegbot_cursor.fetchall()
        if len(sql_search_results) == 1:
            if verbosity>1:
                print "Found %s %s with badge_id: %s and balance: $%.2f" % (sql_search_results[0][1], sql_search_results[0][2], sql_search_results[0][7], sql_search_results[0][5])
            open_session(sql_search_results, verbosity)
        # If nobody (or multiple people) has this ID, generate log, etc.
        else:
            kb_functions.report_bad_login(len(sql_search_results), badge_id, admins, verbosity) # need to expand this function
            if verbosity>0:
                print "Bad login! Found %s users with badge id %s\n" % (len(sql_search_results), badge_id)
        if verbosity>2:
            print "active_session now %s" % active_session
    else:
        if verbosity>2:
            print "Swipe pressed! Session_active is currently %s. Will now try to close session" % active_session
        close_session()
        if verbosity>2:
            print "active_session now %s" % active_session


def open_session(sql_search_results, verbosity):
    #from datetime import datetime
    global active_session
    global current_user
    global session_time
    global loginTime
    global tap_list
    try:
        tap_list = kb_functions.build_taps_list(kegbot_cursor, sql_schema, sql_taps_table, sql_kegs_table, verbosity)
    except:
        print "WOAHHH! Can't get SQL table for taps"
    if verbosity>2:
        kb_functions.print_taps_table(tap_list)
    # Assign SQL search results to current_user attributes
    current_user.user_id = int(sql_search_results[0][0])
    current_user.first_name = str(sql_search_results[0][1])
    current_user.last_name = str(sql_search_results[0][2])
    current_user.is_employee = bool(sql_search_results[0][3])
    current_user.is_admin = bool(sql_search_results[0][4])
    current_user.balance = float(sql_search_results[0][5])
    current_user.email = str(sql_search_results[0][6])
    current_user.badge_id = str(sql_search_results[0][7])
    current_user.nfc_id = str(sql_search_results[0][8])
    if verbosity>1:
        print "User %s %s logged in" % (current_user.first_name, current_user.last_name)
    session_timed_out = False
    session_time = datetime.datetime.now()
    active_session = True
    # Reset flow counters
    for key in flow_pins:
        flow_pins[key] = 0
    login_time = session_time
    kb_functions.new_session(login_time, current_user, flow_counters, verbosity)
    enable_disable_taps(False, tap_list, verbosity)
    try:
        message = """Welcome, %s<br />" +
            "Your remaining balance is $%.2f<br />" +
            "Insert cash when the guest balance falls to $0.00""" % (current_user.user_id, current_user.balance)
        kb_functions.post_web_message(web_server_ip, 'message', message)
        current_web_message = 'login'
        #print kb_functions.post_web_message(web_server_ip, 'login', current_user.user_id)
        #current_web_message = 'login'
    except:
        pass
    if verbosity>0:
        if current_user.user_id == 1: # guest user
            print "%s logged IN at %s with starting balance: $%.2f\n" % (current_user.first_name, datetime.datetime.now().strftime('%A %B %d, %Y at %I:%M%p'), current_user.balance)
        else:
            print "%s %s logged IN at %s with starting balance: $%.2f\n" % (current_user.first_name, current_user.last_name, datetime.datetime.now().strftime('%A %B %d, %Y at %I:%M%p'), current_user.balance)


def close_session():
    global active_session
    global current_user
    global admins
    global session_timed_out
    global kegbot_cursor
    global open_kegbot_schema
    global session_time
    
    # Close current session
    if verbosity>0:
        print "%s logged OUT at %s.\n" % (current_user.first_name, datetime.datetime.now().strftime('%A %B %d, %Y at %I:%M%p'))
    active_session = False
    
    # Disable all taps
    enable_disable_taps(True, tap_list, verbosity)
    
    # Finalize session log
    kb_functions.finalize_session(kb_email_addr, smtp_server, session_time, admins, current_user, flow_counters, tap_list, cts_per_oz, session_timed_out, web_server_ip, sql_schema, sql_trans_table, kegbot_cursor, verbosity)
    current_web_message = 'logout'
    
    # Email pour report to user and admins
    print "Emailing pour report now..."
    kb_functions.email_pour_report(kb_email_addr, smtp_server, current_user, admins, mail_server, verbosity)
    session_timed_out = False

    session_time = datetime.datetime.now()
    # Reset tap counters
    for key in flow_counters:
        flow_counters[key] = 0
    print "Waiting for swipe..."
    
    # When user logs out, refresh the web page
    #try:
    kb_functions.post_web_message(web_server_ip, 'refresh')
    #except:
    #    pass

#####################################################
# Program Setup
#####################################################

try:
    tap_list = kb_functions.build_taps_list(kegbot_cursor, sql_schema, sql_taps_table, sql_kegs_table, verbosity)
except:
    print "WOAHHH! Can't get SQL table for taps"
if verbosity>2:
    kb_functions.print_taps_table(tap_list)


# Setup interrupts
GPIO.add_event_detect(PIN_SWIPE, GPIO.FALLING)#, bouncetime=PIN_SWIPE_BOUNCE_TIME_MS)
GPIO.add_event_detect(PIN_GUEST_LOGIN, GPIO.FALLING)#, bouncetime=PIN_GUEST_LOGIN_BOUNCE_TIME_MS)
GPIO.add_event_detect(flow_pins["tap1"], GPIO.FALLING, callback=incr_flow_ctr_tap1)
GPIO.add_event_detect(flow_pins["tap2"], GPIO.FALLING, callback=incr_flow_ctr_tap2)
GPIO.add_event_detect(flow_pins["tap3"], GPIO.FALLING, callback=incr_flow_ctr_tap3)
GPIO.add_event_detect(PIN_BILLREADER, GPIO.RISING, callback=incr_dollars_inserted, bouncetime=BILL_READER_BOUNCE_TIME_MS)


#####################################################
# Main Program Loop
#####################################################

start_time = datetime.datetime.now()
logout_time = datetime.datetime.now()

print "Waiting for swipe..."


message = "You look thirsty. Swipe your badge<br />or push \"GUEST\" button to get started."
current_web_message = 'idle'
#try:
kb_functions.post_web_message(web_server_ip, 'message', message)
#except:
#    pass


try:
    while True:
    
        # COMBINE THESE INTO ONE EVENT?
        
        #print "schema is open: ", open_kegbot_schema.open
        
        if GPIO.event_detected(PIN_SWIPE):
            # Get badge ID - Normally returned from reader
            badge_id = raw_input("Enter badge ID (ken=111): ")
            toggle_user_session()
        
        #print "current_web_message = ", current_web_message
        
        if GPIO.event_detected(PIN_GUEST_LOGIN):
        #if current_web_message == 'idle':
            #if raw_input("enter 99999 for to toggle guest session: ") == "99999": # This was for testing remotely
            if verbosity>2:
                kb_functions.print_taps_table(tap_list)
            badge_id = "99999"
            current_web_message = 'login'
            #time.sleep(0.4) # somewhat of a software debounce after guest button is pressed
            toggle_user_session()
        
        #print "schema is open: ", open_kegbot_schema.open
        
        # Calculate seconds since last activity
        current_time = datetime.datetime.now()
        time_elapsed = current_time - session_time #datetime.datetime.now()
        sec = (time_elapsed.days * 24 * 60 * 60 + time_elapsed.seconds)
        
        # Every minute, update temperature on webpage
        #print "int(round(time.time())) = ", int(round(time.time()))
        cur_secs = int(round(time.time()))
        if cur_secs % 20 == 0: #int(round(time.time())) % 30 == 0:
            #print "UPDATING TEMPERATURE!"
            #try:
            kb_functions.post_web_message(web_server_ip, 'temperature')
            #except:
            #    pass
        
        # A non-guest user is logged in
        if active_session == True:
            # Check time elapsed since last user activity
            # Clear timeout warning message if user becomes active again
            if sec < session_timeout_warning and current_web_message == "warn":
                try:
                    message = """Welcome, %s<br />" +
                        "Your remaining balance is $%.2f<br />" +
                        "Insert cash when the guest balance falls to $0.00""" % (current_user.user_id, current_user.balance)
                    kb_functions.post_web_message(web_server_ip, 'message', message)
                    current_web_message = 'login'
                    #kb_functions.post_web_message(web_server_ip, 'login', current_user.user_id)
                except:
                    pass
            # Display timeout warning message if user is inactive for some period of time set in kegbot_config.yaml
            elif sec == session_timeout_warning:
                session_timeout_warn_time = session_timeout - session_timeout_warning
                try:
                    
                    message = """ Your session is about to expire due to inactivity</br>
                                Please logout or, to keep your session active,</br>
                                insert bills or pour beer"""
                    kb_functions.post_web_message(web_server_ip, 'message', message)
                    current_web_message = 'warn'
                    #kb_functions.post_web_message(web_server_ip, 'timeout_warn', session_timeout_warn_time)
                except:
                    pass
            # Log user out after a period of inactivity
            elif sec >= session_timeout:
                session_timed_out = True
                current_web_message = 'logout'
                close_session()
        # time since logout
        else:
            if sec == display_idle_time:
                try:
                    message = "You look thirsty.<br />Swipe your badge or push \"GUEST\" button to get started."
                    kb_functions.post_web_message(web_server_ip, 'message', message)
                    current_web_message = 'idle'
                except:
                    pass
        
        time.sleep(1)
        
        # This is here just as a test:
        
        if not open_kegbot_schema.open:
            print "SQL DATABASE CONNECTION SOMEHOW CLOSED!!!!!!!!!  AAAAHHHHHHHHH!!!!!!!!!!!"
        
        
except KeyboardInterrupt:
    GPIO.cleanup()
    if open_kegbot_schema.open:
        try:
            print "Keyboard interrupt - Closing SQL Table and Cursor, also closing mail_server"
            kegbot_cursor.close()
            open_kegbot_schema.close()
        except:
            print "ERROR CLOSING SQL CURSOR/SCHEMA"
    try:
        mail_server.quit()
    except:
        print "ERROR CLOSING MAIL SERVER"



# Unassign all I/O at termination
GPIO.cleanup()
if open_kegbot_schema.open:
    try:
        print "Keyboard interrupt - Closing SQL Table and Cursor, also closing mail_server"
        kegbot_cursor.close()
        open_kegbot_schema.close()
    except:
        print "ERROR CLOSING SQL CURSOR/SCHEMA"
try:
    mail_server.quit()
except:
    print "ERROR CLOSING MAIL SERVER"
