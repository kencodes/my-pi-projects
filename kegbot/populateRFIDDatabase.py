#!/usr/bin/env python2


import os
import MySQLdb
import yaml

os.chdir('/home/pi')

kegbot_config = open("/home/pi/kegbot_config.yaml", 'r')
kegbot_config_yaml = yaml.load(kegbot_config)
kegbot_config.close()

sql_host = '192.168.201.184'
sql_user = 'pi'
sql_passwd = 'kegbot'
sql_schema = 'test'
sql_df_users_table = 'df_users'

open_schema = MySQLdb.connect(sql_host, sql_user, sql_passwd, sql_schema)
open_schema.autocommit(True)
kegbot_cursor = open_schema.cursor()


try:
    while True:

        rfid = raw_input("Swipe your RFID tag: ")
        first_name = raw_input("Type your first name: ")
        last_name = raw_input("Type your last name: ")
        camp = 'asdf'
        while not (camp == 'DF' or camp == 'LP' or camp == ''):
            camp = raw_input("What camp are you in? Enter DF, LP or just hit enter for neither: ")
            if camp == '':
                camp = 'UNKNOWN'
                break
            elif camp == 'DF':
                break
            elif camp == 'LP':
                break
        email = raw_input("Enter your email address or just hit enter to finish registering: ")
        
        try:
            kegbot_cursor.execute("""INSERT INTO `%s`.`%s` (`first_name`, `last_name`, `camp`, `rfid`, `email`)
            VALUES ('%s', '%s', '%s', '%s', '%s');""" % (sql_schema, sql_df_users_table, first_name, last_name, camp, rfid, email))
        except:
            print "There was an error"
        
        print "\nThank you for registering.\nYou will now be able to drink cold beer on the Playa!\n"
        
except KeyboardInterrupt:
    if open_schema.open:
        try:
            print "Keyboard interrupt - Closing SQL Table"
            kegbot_cursor.close()
            open_schema.close()
        except:
            print "ERROR CLOSING SQL CURSOR/SCHEMA"
